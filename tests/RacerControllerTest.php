<?php

use PHPUnit\Framework\TestCase;

require 'Controller/Controller.php';
require 'Controller/RacerController.php';
require 'Controller/CarsController.php';
require 'Controller/CarController.php';

final class RacerControllerTest extends TestCase
{

    public function testValidTrack(): void
    {
        $testRacer = new Controller\RacerController();

        $this->assertEquals(true, $testRacer->validateTrack('1011110'));
        
    }


}