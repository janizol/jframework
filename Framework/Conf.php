<?php

namespace Framework;

abstract class Conf
{
    public function loadConf($filename){
        if(!file_exists($filename)){
            if(!file_exists($filename.'.sample')){
                print "No config loaded";
                exit();
            } else {
                $conf_file = $filename.'.sample';
            }
        } else {
            $conf_file = $filename;
        }
        $conf_string = file_get_contents($conf_file);
        return explode(',',$conf_string);
    }
}

?>