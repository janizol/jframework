<?php

namespace Framework\Modules;

require_once('Framework/Conf.php');

class Modules extends \Framework\Conf
{
    public $module;

    function __construct(){
        $this->loadModules();
    }

    private function loadModules(){
        $this->module = $this->loadConf('Framework/conf/module.conf');
    }


}
?>