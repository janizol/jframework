<?php
/**
 * Created by PhpStorm.
 * User: janizoladams
 * Date: 2018/03/18
 * Time: 17:58
 */

namespace Framework\DB;

require_once('Framework/DB/Database.php');

class Connection extends Database
{
    private static $_instance; //The single instance

    function __construct($filename) {
        if (!$this->getInstance()){
            parent::__construct($filename);
            try {

                $this->db = new \PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pwd);
            }
            catch (\PDOException $e) {
                echo $e->getMessage();
            }
        }
    }

    /*
	Get an instance of the Database
	@return boolean
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			return false;
		}
		return true;
	}

    public function insert($table,$array = array(),$date_field = null) {

        $columns=array_keys($array[0]);
        $values=array_values($array[0]);

        if(isset($date_field)){
            $str="INSERT INTO ".$table." (".implode(',',$columns).", $date_field) VALUES ('" . implode("', '", $values) . "', NOW() )";
        } else {
            $str="INSERT INTO ".$table." (".implode(',',$columns).") VALUES ('" . implode("', '", $values) . "' )";
        }

        $insert = $this->db->prepare($str);
        return $insert->execute();

    }

    public function update($table,$array = array()) {

        $update_query = "";

        foreach($array[0] as $columns => $values){
            $update_query .= $columns." = '".$values."',";
        }

        $update_query = substr($update_query, 0, -1);

        $str="UPDATE ".$table." SET ".$update_query. " WHERE id = ".$array[0]['id'];
        $update = $this->db->prepare($str);
        return $update->execute();

    }

    public function get($table,$id){
        $get = $this->db->prepare("SELECT * FROM ".$table." WHERE id = ".$id);
        $get->execute();

        $result = $get->fetchAll();
        return $result;
    }

    public function getLastRow($table){
        $get = $this->db->prepare("SELECT * FROM ".$table." ORDER BY id DESC LIMIT 0, 1");
        $get->execute();

        $result = $get->fetchAll();
        return $result;
    }

    public function getAll($table, $rows = null){
        $query = "SELECT * FROM ".$table;
        if(isset($rows) && is_numeric($rows)){
            $query = $query." ORDER BY id DESC LIMIT 0, 10" ;
        }
        $get = $this->db->prepare($query);

        $get->execute();

        $result = $get->fetchAll();
        return $result;
    }

    public function delete($table,$id) {
        $delete = $this->db->prepare("DELETE FROM ".$table." WHERE id = ".$id);
        return $delete->execute();
    }

    public function closeConnection() {

        $this->db = null;
    }
}