<?php
/**
 * Created by PhpStorm.
 * User: janizoladams
 * Date: 2018/03/20
 * Time: 21:02
 */

namespace Framework\DB;

interface Details
{
    public function setHost($host);

    public function setDB($dbname);

    public function setUserName($user);

    public function setPassword($pwd);

}