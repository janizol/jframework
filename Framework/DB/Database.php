<?php
/**
 * Created by PhpStorm.
 * User: janizoladams
 * Date: 2018/03/20
 * Time: 21:07
 */

namespace Framework\DB;

require_once('Framework/Conf.php');
require_once('Framework/DB/Details.php');

abstract class Database extends \Framework\Conf implements Details
{
    protected $host;
    protected $dbname;
    protected $user;
    protected $pwd;

    function __construct($filename) {
        $conf = $this->loadConf($filename);
        $this->setHost($conf[0]);
        $this->setDB($conf[1]);
        $this->setUserName($conf[2]);
        $this->setPassword($conf[3]);
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function setDB($dbname)
    {
        $this->dbname = $dbname;
    }

    public function setUserName($user)
    {
        $this->user = $user;
    }

    public function setPassword($pwd)
    {
        $this->pwd = $pwd;
    }


}