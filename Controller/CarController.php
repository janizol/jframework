<?php

namespace Controller;

class CarController implements \Interfaces\CarInterface
{

    protected $name;
    protected $acceleration;
    protected $topSpeed;
    protected $cornering;

    function __construct($car = array()){
        $this->setName($car[0]);
        $this->setAcceleration($car[1]);
        $this->setTopSpeed($car[2]);
        $this->setCornering($car[3]);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function setAcceleration($acceleration)
    {
        $this->acceleration = $acceleration;
    }

    public function getAcceleration(){
        return $this->acceleration;
    }

    public function setTopSpeed($topSpeed)
    {
        $this->topSpeed = $topSpeed;
    }

    public function getTopSpeed(){
        return $this->topSpeed;
    }

    public function setCornering($cornering)
    {
        $this->cornering = $cornering;
    }

    public function getCornering(){
        return $this->cornering;
    }

}