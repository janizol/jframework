<?php

namespace Controller;

class RacerController extends Controller
{
    protected $track;
    protected $cars;

    function __construct(){
        $this->track = $this->loadTrack();

        $cars_array = array(array('A',5,10,2),array('B',3,9,6),array('C',3,15,3),array('D',7,12,3));
        $this->loadCars = new \Controller\CarsController($cars_array);
    }

    public function indexAction(){

        $racer = $this;

        // Load View template

        include "View/Racer/index.html";

    }

    public function race(){
        //each track char
        $trackLength = strlen($this->track);
        $carsArr = $this->loadCars->getCars();
        for( $i = 0; $i <= $trackLength-1; $i++ ) {
            $character = substr( $this->track, $i, 1 );
            // echo "First Char ".$character;
            //each car
            $cnt = 0;
            foreach($carsArr as $car){
                // echo $car['trackTotal'];
                if(empty($car['trackTotal'])){
                    $car['trackTotal'] = 0;
                }
                if(empty($car['speed'])){
                    $car['speed'] = 0;
                }
                //echo $car['trackTotal'];
                if($character == 0){
                    $car['speed'] = $this->speedLimiter($car,$this->calculateStraight($car));
                } else {
                    $car['speed'] = $this->calculateBend($car);
                }
                $car['trackTotal'] = $car['speed'] + $car['trackTotal'];
                $car['sum'][] = $car['speed'];
                $carsArr[$cnt] = $car;
                $cnt++;
            }
        }

        return $this->getRaceResults($carsArr);

        // echo '<pre>';
        //     print_r($carsArr);
        // echo '</pre>';
    }

    private function calculateStraight($car){
        if($car['speed'] < $car['topSpeed']){
            return $car['acceleration'];
        }
    }

    private function calculateBend($car){
        return $car['cornering'];
    }

    private function speedLimiter($car, $calculationAmount){
        $unlimitedTotal = $car['speed']+$calculationAmount;
        if($unlimitedTotal > $car['topSpeed']){
            return $car['topSpeed'];
        } else {
            return $unlimitedTotal;
        }
    }

    private function cornerCar($car){
        $car['trackTotal'] = $car['trackTotal'] + $car['cornering'];
    }

    private function getPreviousCalculation($car){
        //is there a previous calculation
        if(empty($car['previousCalculation'])){
            return false;
        } else {
            return $car['previousCalculation'];
        }
    }

    public function validateTrack($track){
        //strip whitespace
        $track = str_replace(' ','',$track);
        //check that it only contains 1 and 0
        return $this->checkCharacters($track);
    }
    
    private function checkCharacters($track){
        if (preg_match('/^[0-1]*$/',$track)) {
            return true;
        } else {
            return false;
        }
    }

    private function loadTrack(){
        $track = file_get_contents('track.txt');
        return $track;
    }

    public function getTrack(){
        return $this->track;
    }

    private function getRaceResults($cars){
        $totals = array();
        foreach ($cars as $key => $row)
        {
            $totals[$key] = $row['trackTotal'];
        }
        array_multisort($totals, SORT_DESC, $cars);

        return $cars;
    }
    
}