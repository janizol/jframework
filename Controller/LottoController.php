<?php

namespace Controller;

class LottoController extends Controller
{

    function __construct($con){
        $this->lotto = new \Model\Lotto($con);
    }

    public function indexAction(){

        $numbers = $this->lotto->getAllNumbers(10);

        // Load View template

        include "View/Lotto/index.html";

    }

    public function draw(){
        $main = $this->drawMain();
        $powerball = $this->drawPowerBall();
        $numbers = array('numbers' => $main, 'powerball_numbers' => $powerball);
        $this->storeResults($numbers);
        return $this->lotto->getAllNumbers(10);
    }

    private function drawMain(){
        return $this->getBalls(7);
    }

    private function drawPowerBall(){
        return $this->getBalls(3);
    }

    private function getBalls($numBalls){
        $number = array();
        for($i = 0; $i <= ($numBalls-1); $i++){
            $new_number = $this->getNumber(1, 49, $number);
            $number[] = $new_number;
        }
        return $number;
    }

    private function getNumber($start, $end, $taken = array()){
        do {   
            $num = rand($start,$end);
        } while(in_array($num, $taken));
        return $num;
    }

    private function storeResults($array){
        $arrays[] = array(
            'numbers' => implode(",", $array['numbers']),
            'powerball_numbers' => implode(",", $array['powerball_numbers']),
        );
        $this->lotto->addNumbers($arrays, 'date');
    }

}