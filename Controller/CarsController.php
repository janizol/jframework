<?php

namespace Controller;

class CarsController extends \Controller\CarController
{
    public $_cars;

    function __construct($cars = array()){
        foreach($cars as $car){
            $this->_cars[] = new \Controller\CarController($car);
        }
    }

    function getCars(){
        $cars = array();
        foreach($this->_cars as $car){
            $cars[] = array(
                'name' => $car->getName(), 
                'acceleration' => $car->getAcceleration(), 
                'topSpeed' => $car->getTopSpeed(), 
                'cornering' => $car->getCornering()
            );
        }

        return $cars;
    }
}