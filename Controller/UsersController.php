<?php
/**
 * Created by PhpStorm.
 * User: janizoladams
 * Date: 2018/03/21
 * Time: 09:30
 */

namespace Controller;

class UsersController extends Controller
{

    public function indexAction($con){

        $user  = new \Model\Users($con);

        $users = $user->getUsers();

        // Load View template

        include "View/Users/index.html";

    }

}