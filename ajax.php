<?php
//echo json_encode("IN");die;
require_once('autoloader.php');

$con  = new Connection($conf[0], $conf[1], $conf[2], $conf[3]);
$users  = new Users($con);

$user_array = array($_POST);

unset($user_array[0]['function']);
switch($_POST['function']){
    case 'add':
        $return = $users->addUser($user_array);
        break;
    case 'edit':
        $return = $users->updateUser($user_array);
        break;
    case 'delete':
        $return = $users->deleteUser($user_array[0]['id']);
        break;
    case 'getUser':
        $return = $users->getUser($user_array[0]['id']);
        break;
    case 'getUsers':
        $return = $users->getUsers();
        break;
    default:
        $return = false;
        break;
}

echo json_encode($return);