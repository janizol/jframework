<?php

namespace Model;

class Lotto extends \Framework\DB\Connection
{

    function __construct(\Framework\DB\Connection $connection){
        parent::__construct('db.conf');
    }

    public function addNumbers($array = array(),$date_field) {

        return $this->insert('lotto',$array,$date_field);

    }

    public function getNumber($id){

        return $this->get('lotto',$id);

    }

    public function getAllNumbers($rows = null){

        return $this->getAll('lotto', $rows);

    }

    public function updateNumbers($array = array()){

        return $this->update('lotto',$array);

    }

    public function deleteNumbers($id){

        return $this->delete('lotto',$id);

    }

    public function getLastNumbers($table){

        return $this->getLastRow('lotto');

    }
}