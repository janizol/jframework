<?php
/**
 * Created by PhpStorm.
 * User: janizoladams
 * Date: 2018/03/18
 * Time: 18:00
 */
namespace Model;

class Users extends \Framework\DB\Connection
{

    function __construct(\Framework\DB\Connection $connection){
        parent::__construct('db.conf');
    }

    public function addUser($array = array()) {

        return $this->insert('users',$array);

    }

    public function getUser($id){

        return $this->get('users',$id);

    }

    public function getUsers(){

        return $this->getAll('users');

    }

    public function updateUser($array = array()){

        return $this->update('users',$array);

    }

    public function deleteUser($id){

        return $this->delete('users',$id);

    }
}