<?php
require_once('autoloader.php');

$lottoController  = new Controller\LottoController($con);

switch($_POST['function']){
    case 'drawNumbers':
        $return = $lottoController->draw();
        break;
    default:
        $return = false;
        break;
}

echo json_encode($return);