<?php
/**
 * Created by PhpStorm.
 * User: janizoladams
 * Date: 2018/03/18
 * Time: 18:36
 */

require_once('Framework/DB/Connection.php');
require_once('Framework/Modules/Modules.php');
require_once('Controller/Controller.php');

$modules = new Framework\Modules\Modules;

foreach($modules->module as $module){
    require_once('Model/'.$module.'.php');
    require_once('Controller/'.$module.'Controller.php');
}

require_once('Interfaces/CarInterface.php');
require_once('Controller/CarController.php');
require_once('Controller/CarsController.php');

$con = new Framework\DB\Connection('db.conf');