<?php

namespace Interfaces;

interface CarInterface
{

    public function setName($name);

    public function setAcceleration($acceleration);

    public function setTopSpeed($topSpeed);

    public function setCornering($cornering);

}